/******************************************************************************************
* File Name          : JAFR.ino
* Author             : Mike Schoonover
* Version            : 1.0
* Date               : 10/19/2017
* Description        : JAFR robot control code.
* License            : CC-BY-SA 3.0
* License Info       : https://creativecommons.org/licenses/by-sa/3.0/deed.en
* Author Website     : http://www.somedisassemblyrequired.com
* 
* Partially Derived from:
* 
*   File Name          : mbot_factory_firmware.ino
*   Author             : Ander, Mark Yan
*   Updated            : Ander, Mark Yan
*   Version            : V06.01.007
*   Date               : 07/06/2016
*   Description        : Firmware for Makeblock Electronic modules with Scratch.  
*   License            : CC-BY-SA 3.0
*   Copyright (C) 2013 - 2016 Maker Works Technology Co., Ltd. All right reserved.
*   http://www.makeblock.cc/ 
* 
****************************************************************************************
*
* To compile for the mBot with mCore processor board in the Arduino IDE, select:
*   Tools/Board/Arduino/Genuino
*
* It doesn't matter which programmer is selected...that is only used for burning the
* bootloader.
*
* Functional Description 
* 
* On startup: 
* 
*  flashes right-side LED to demonstrate max and max/2 brightness.
*  cycles left-side LED through the RGB color range
* 
* Four modes are implemented:
*  
* Mode A (press A button on remote)
*  robot is controlled by the remote ~ forward, reverse, left, right, speeds 1-9
*
* Mode B (B button)
*  robot does a dance jig to test motors
*  
* Mode C (C button)
*  robot will follow a black line on white paper  
*  
* Mode D (D button)
*  robot will move around a table top until it reaches an edge, then it will
*   reverse and turn
*
*
* Infrared IR Remote Control Notes
*
* Empirical testing shows that the IR remote transmits a code when a button
* is first pressed and then sends a repeat signal when the button is held
* down. If the MBot receiver misses the code for a button press, it will
* still respond to the repeat signal which is the same for all buttons
* but will use the code from the previous button press. Moving the code which sets the
* RGB LEDs such that they are only set between mode/direction changes made the IR
* remote work nearly perfectly.
*
* As of 6/16/2017, it is unknown if the above behavior is hardwired into the circuitry
* or is a part of the driver software.
*
*****************************************************************************************/

#include <MeMCore.h>

const int SILENT = true;

#define NTD1 294
#define NTD2 330
#define NTD3 350
#define NTD4 393
#define NTD5 441
#define NTD6 495
#define NTD7 556
#define NTDL1 147
#define NTDL2 165
#define NTDL3 175
#define NTDL4 196
#define NTDL5 221
#define NTDL6 248
#define NTDL7 278
#define NTDH1 589
#define NTDH2 661
#define NTDH3 700
#define NTDH4 786
#define NTDH5 882
#define NTDH6 990
#define NTDH7 112

#define STOP      0
#define FORWARD   1
#define REVERSE   2
#define LEFT      3
#define RIGHT     4
#define STRAIGHT  5

#define MODE_A    1
#define MODE_B    2
#define MODE_C    3
#define MODE_D    4
#define MODE_E    5
#define MODE_F    6


//define the commands sent by the controller

#define REQUEST_DATA_CMD 0
#define SET_HEAD_ANGLE_CMD 1
#define SET_LED_DIGIT_DISPLAY_CMD 2
#define SET_LED_DIGIT_DISPLAY_DIRECT_CMD 3
#define SET_DISPLAY_STATUS_FLAGS_CMD 4

//define bits in the Display Select byte

#define CHECKSUM_ERROR_CNT 0x01
#define PACKET_RCVD_CNT 0x02


// The motor won't rotate quite 90 degrees to the left. Spec sheet says it is only rated for 120 degrees (60 degrees +/- from center), so that makes sense.
// For some reason, it will rotate more than 60 degrees right.
// These constants give reasonable positions of FORWARD, LEFT, QUARTER LEFT, RIGHT, AND QUARTER RIGHT.

#define EYES_FORWARD 90
#define EYES_LEFT 180
#define EYES_Q_LEFT 135
#define EYES_RIGHT 0
#define EYES_Q_RIGHT 45

#define MAX_ULTRASONIC_DISTANCE 25

typedef uint8_t  U_BYTE;
typedef uint32_t U_DWORD;
typedef int32_t  LONG;
typedef uint16_t WORD;

const U_BYTE RIGHT_RGB_LED = 0;
const U_BYTE LEFT_RGB_LED = 1;

const int MOVE_SPEED_MIN = 100;
const int MOVE_SPEED_MID = 175;
const int MOVE_SPEED_MAX = 255;

const int moveSpeedChangeStep = 23;

boolean currentPressed = false;
boolean prevButtonState = false;

#define EYE_MOVE_TIME_VALUE 10

U_DWORD eyeMoveTimer;

U_BYTE eyePosition;

MeIR ir;

MeUltrasonicSensor ultr(PORT_4);
MeLineFollower edgeDetector(PORT_2);

int lineFollowFlag=0;

MeBuzzer buzzer;

MeRGBLed rgb(0,16);

MeDCMotor MotorL(M1);
MeDCMotor MotorR(M2);

Me7SegmentDisplay digitDisplay(PORT_1);

Servo frontUTServo;

MePort port(PORT_3);

int16_t servo1pin =  port.pin1();//attaches the servo on PORT_3 SLOT1 to the servo object
int16_t servo2pin =  port.pin2();//attaches the servo on PORT_3 SLOT2 to the servo object

int moveSpeed = MOVE_SPEED_MIN;

U_BYTE motorState = STOP;
U_BYTE prevMotorState = STOP;
U_BYTE mode = MODE_A;

U_BYTE moveMode = 0x0a;

int turnTrack = LEFT;

int distFwd, distLeft, distQLeft, distRight, distQRight;

int serialInBufStorePos = 0;
int serialInBufReadPos = 0;
int serialInBufCount = 0;
int pktCmd;
int pktReady = FALSE;

char statusDisplayFlags = 0;
int checksumErrorCnt = 0;
int packetRcvdCnt = 0;

#define PACKET_SIZE 8
#define SERIAL_IN_BUF_SIZE 128
#define DATUM_SIZE 4

int serialInBuf[SERIAL_IN_BUF_SIZE] = {0};
int datum[DATUM_SIZE] = {0};

//------------------------------------------------------------------------------------------
// writeSerial
//
// Writes data to the serial port.
//

void writeSerial(uint8_t pData){

  Serial.write(pData);

}//writeSerial
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// displayStatus
//
// Displays a status value on the LED digit display.
//
// The status displayed is controlled by the bits of statusDisplayFlags.
//

void displayStatus(){

  if ((statusDisplayFlags & CHECKSUM_ERROR_CNT) != 0){
    digitDisplay.display(checksumErrorCnt);
  }

  if ((statusDisplayFlags & PACKET_RCVD_CNT) != 0){
    digitDisplay.display(packetRcvdCnt);
  }

}//end of displayStatus
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// handleSetDisplayStatusFlags
//
// Handles SET_DISPLAY_STATUS_FLAGS_CMD command which sets the statusDisplayFlags byte
// from a packet from the host. The display is updated to reflect the change.
//
// The statusDisplayFlags byte controls which information is displayed on the LED Digit
// Display.
//

void handleSetDisplayStatusFlagsCmd(){

  statusDisplayFlags = datum[0];

  displayStatus();

}//end of handleSetDisplayStatusFlags
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// soundBuzzer
//
// Sounds the buzzer at frequency pFreqency for duration pDuration.
//
// Most MakeBlock example code disables interrupts during buzzer...necessary?

void soundBuzzer(int pFrequency, int pDuration)
{

  if (SILENT) {return;}

  cli();
  buzzer.tone(pFrequency, pDuration);
  sei();

}// end of soundBuzzer
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setServoPosition
//
// Sets the servo at position pPosition.
//

void setServoPosition(int pPosition)
{

  frontUTServo.write(pPosition);

}// end of setServoPosition
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// bufferSerialInput
//
// Reads data from the serial port and stores it in a circular buffer.
//

void bufferSerialInput()
{

  int readData;

  if ((readData = Serial.read()) == -1){ return; }

  serialInBufCount++;

  serialInBuf[serialInBufStorePos++] = readData;

  if (serialInBufStorePos == SERIAL_IN_BUF_SIZE) { serialInBufStorePos = 0; }

}// end of bufferSerialInput
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// getByteFromSerialInBuf
//
// Gets the next byte from the circular buffer serialInBuf. The read pointer and byte
// count variables are adjusted.
//
// Returns the byte or -1 if no bytes are available.
//

int getByteFromSerialInBuf(){

  int data;

  if(serialInBufCount <= 0) { return(-1); }

  serialInBufCount--;

  data = serialInBuf[serialInBufReadPos++];

  if (serialInBufReadPos == SERIAL_IN_BUF_SIZE) { serialInBufReadPos = 0; }

  return(data);

}// end of getByteFromSerialInBuf
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// processPacket
//
// If the number of bytes in the serialInBuf is equal to PACKET_SIZE, a packet is
// processed.
//
// If the packet header is invalid then the remaining bytes after the first non-valid
// header byte will be left in the buffer. The next call will attempt to extract a packet
// from the remaining bytes.
//
// On success, returns number of byes read with pktCmd set to the packet's command byte
// and pktReady is set to TRUE.
// On error, returns -1 with pktCmd undefined and pktReady is set to FALSE.
//

int processPacket()
{

  int i, data, checksum = 0;

  pktReady = FALSE;

  if(serialInBufCount < PACKET_SIZE){ return(-1); }

  displayStatus();

  //extract and validate 0xaa55 header bytes

  data = getByteFromSerialInBuf();
  if (data != 0xaa) { return(-1); }
  data = getByteFromSerialInBuf();
  if (data != 0x55) { return(-1); }

  //get the packet command byte
  pktCmd = getByteFromSerialInBuf();

  //get packet data
  datum[0] = getByteFromSerialInBuf();
  datum[1] = getByteFromSerialInBuf();
  datum[2] = getByteFromSerialInBuf();
  datum[3] = getByteFromSerialInBuf();

  checksum = pktCmd;

  for(i=0; i<DATUM_SIZE; i++){
    checksum+= datum[i];
  }
  
  //calculate checksum
  checksum = (0x100 - (byte)(checksum & 0xff));

  if(checksum != getByteFromSerialInBuf()) { checksumErrorCnt++; return (-1); }

  packetRcvdCnt++;

  pktReady = TRUE;

  return(PACKET_SIZE);

}// end of processPacket
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// handleSetHeadAngleCmd
//
// Handles the SET_HEAD_ANGLE_CMD received from serial port. First byte in the packet
// (stored in datum[0] specifies the angle.
//

void handleSetHeadAngleCmd()
{

    setServoPosition(datum[0]);

}// end of handleSetHeadAngleCmd
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setMoveSpeed
//
// Sets the moving speed to pSpeed.
//
// Restricts upper limit to MOVE_SPEED_MAX.
// No lower limit enforced...too low of a value may not move the wheel.
//

void setMoveSpeed(int pSpeed)
{

  moveSpeed = pSpeed;

  if(moveSpeed > MOVE_SPEED_MAX) { moveSpeed = MOVE_SPEED_MAX; }

}// end of setMoveSpeed
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setRGBLEDsFwdMode
//
// Sets the RGB LEDs to indicate forward motion.
//

void setRGBLEDsFwdMode(){

  rgb.setColor(0,0,0);     //set all LEDs off
  rgb.setColor(10, 10, 0); //set all LEDs to Yellow
  rgb.show();

}// end of setRGBLEDsFwdMode
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setRGBLEDsRevMode
//
// Sets the RGB LEDs to indicate reverse motion.
//

void setRGBLEDsRevMode(){

  rgb.setColor(0,0,0);     //set all LEDs off
  rgb.setColor(10, 0, 0);  //set all LEDs Red
  rgb.show();

}// end of setRGBLEDsRevMode
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setRGBLEDsLeftMode
//
// Sets the RGB LEDs to indicate left forward turn.
//

void setRGBLEDsLeftMode(){

  rgb.setColor(0,0,0);        //set all LEDs off
  rgb.setColor(2,10, 10, 0);  //set left LED Yellow
  rgb.show();

}// end of setRGBLEDsLeftMode
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setRGBLEDsRightMode
//
// Sets the RGB LEDs to indicate right forward turn.
//

void setRGBLEDsRightMode(){

  rgb.setColor(0,0,0);        //set all LEDs off
  rgb.setColor(1,10, 10, 0);  //set right LED Yellow
  rgb.show();

}// end of setRGBLEDsRightMode
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setRGBLEDsLeftRevMode
//
// Sets the RGB LEDs to indicate left reverse turn.
// Front of bot will be turned left while backing.
//

void setRGBLEDsLeftRevMode(){

        rgb.setColor(0,0,0);     //set all LEDs off
        rgb.setColor(2,10, 0, 0);  //set left LED Yellow
        rgb.show();

}// end of setRGBLEDsLeftRevMode
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setRGBLEDsRightRevMode
//
// Sets the RGB LEDs to indicate right reverse turn.
// Front of bot will be turned right while backing.
//

void setRGBLEDsRightRevMode(){

        rgb.setColor(0,0,0);     //set all LEDs off
        rgb.setColor(1,10, 0, 0);  //set right LED Yellow
        rgb.show();

}// end of setRGBLEDsRightRevMode
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setRGBLEDsStopMode
//
// Sets the RGB LEDs to indicate motion stopped.
//

void setRGBLEDsStopMode(){

  rgb.setColor(0,0,0);     //set all LEDs off
  rgb.show();

}// end of setRGBLEDsStopMode
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// stopMoving
//
// Stops moving by setting all motor speeds to 0.
//

void stopMoving()
{

  setRGBLEDsStopMode();

  MotorL.run(0); MotorR.run(0);

}// end of stopMoving
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// moveFwd
//
// Moves the robot forward by driving both motors at speed of moveSpeed.
//
// The motors are driven in opposite directions as they are mounted on opposite sides, so
// forward and backwards are reversed between the two.
//

void moveFwd(){

  setRGBLEDsFwdMode();

  MotorL.run(-moveSpeed); MotorR.run(moveSpeed);

}// end of moveFwd
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// moveRev
//
// Moves the robot backwards by driving both motors at speed of moveSpeed.
//
// The motors are driven in opposite directions as they are mounted on opposite sides, so
// forward and backwards are reversed between the two.
//

void moveRev(){

  setRGBLEDsRevMode();

  MotorL.run(moveSpeed); MotorR.run(-moveSpeed);

}// end of moveRev
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// turnLeft
//
// Turns the robot left by turning the right wheel forward at moveSpeed and the left
// wheel forward at a slower speed.
//
// Note that at slower vaues of moveSpeed, the slower speed may be too small to actually
// cause the slower wheel to turn...it will appear to be stopped.
//
// Note the motors are mounted in opposite configurations, so forward/reverse are switched
// between them. The sign of the moveSpeed is set to account for this.
//

void turnLeft(){

  setRGBLEDsLeftMode();

  MotorL.run(-moveSpeed/5); MotorR.run(moveSpeed);

}// end of turnLeft
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// turnRight
//
// Turns the robot right by turning the left wheel forward at moveSpeed and the right
// wheel forward at a slower speed.
//
// Note that at slower vaues of moveSpeed, the slower speed may be too small to actually
// cause the slower wheel to turn...it will appear to be stopped.
//
// Note the motors are mounted in opposite configurations, so forward/reverse are switched
// between them. The sign of the moveSpeed is set to account for this.
//

void turnRight(){

  setRGBLEDsRightMode();

  MotorL.run(-moveSpeed); MotorR.run(moveSpeed/5);

}// end of turnRight
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// turnLeftWhileBacking
//
// Turns the robot left while backing by turning the right wheel backward at moveSpeed and
// the left wheel backward at a slower speed.
//
// The motors are turned at the same speed as using a slower speed for one often results
// in difficulty turning.
//
// Note the motors are mounted in opposite configurations, so forward/reverse are switched
// between them. The sign of the moveSpeed is set to account for this.
//

void turnLeftWhileBacking(){

  setRGBLEDsLeftRevMode();

  MotorL.run(moveSpeed); MotorR.run(moveSpeed);

}// end of turnLeftWhileBacking
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// turnRightWhileBacking
//
// Turns the robot right while backing by turning the left wheel backward at moveSpeed and
// the right wheel backward at a slower speed.
//
// The motors are turned at the same speed as using a slower speed for one often results
// in difficulty turning.
//
// Note the motors are mounted in opposite configurations, so forward/reverse are switched
// between them. The sign of the moveSpeed is set to account for this.
//

void turnRightWhileBacking(){

  setRGBLEDsRightRevMode();

  MotorL.run(-moveSpeed); MotorR.run(-moveSpeed);

}// end of turnRightWhileBacking
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setUpRGBLEDs
//

void setUpRGBLeds(){

  pinMode(13,OUTPUT);
  
  digitalWrite(13,HIGH);
  delay(300);
  digitalWrite(13,LOW);
 
  rgb.setpin(13);

  rgb.setColor(0,0,0);
  rgb.show();

}// end of setUpRGBLEDs
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// testRGBLEDs
//
// The LEDs are controlled via an external WS2812 IC chip. The RGB data for the LEDs is
// held internally in a data array. This array is transmitted serially to the WS8212
// to be displayed on the LEDs.
//
// An internal array is created to hold the RGB values for each LED by the constructor:
//    MeRGBLed rgb(0,16);       //WS8212 connected to port 0, creates 16 LED buffer
//
// Functions for setting colors:
//
//    rgb.setColor(r,g,b);      //sets all LEDs to same color
//
//    rgb.setColorAt(n,r,g,b);  //sets nth LED in array to color where n is 0 to ?
//
//    rgb.setColor(n,r,g,b);    //sets (n-1)th LED in array to color where n is 1 to ?
//                              // if n = 0, all LEDs are set to the specified color
//                              // (using setColorAt is probably less confusing)
//
//    rgb.show();               //sends color state for all LEDs to the WS2812 chip
//                              // call this after changing values for one or more LEDs
//

void testRGBLEDs() {


  //set all LEDs to the same color

  rgb.setColor(0,0,0);      //all LEDs off
  rgb.show();
  
  rgb.setColor(10, 10, 10); //all LEDs to same color
  rgb.show();

  delay(2000);

  //flash LED 0 between 100 and 255 to show max brightness level

  for (int i=0; i<2; i++){
    rgb.setColorAt(LEFT_RGB_LED, 100, 100, 100);
    rgb.show();
    delay(200);
    rgb.setColorAt(LEFT_RGB_LED, 255, 255, 255);
    rgb.show();
    delay(200);
  }

  rgb.setColor(10, 10, 10); //all LEDs to same color
  rgb.show();

}// end of testRGBLEDs
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// cycleColorForLED
//
// Cycles through all possible RGB values for LED number pLEDNum.
//
// pLEDNum = 0 or 1
//

void cycleColorForLED(U_BYTE pLEDNum) {

  uint8_t red = 0, green = 0, blue = 0;

  uint8_t maxRGBValue = 100; //absolute max is 255

  for(red = 0; red < maxRGBValue; red+=20){
    for(blue = 0; blue < maxRGBValue; blue+=20){
      for(green = 0; green < maxRGBValue; green+=20){

        rgb.setColorAt(pLEDNum, red, green, blue);
        rgb.show();
        
      }
    }
  }

  delay(100);

  rgb.setColor(10, 10, 10); //all LEDs to same color
  rgb.show();

}// end of cycleColorForLED
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// checkForDropOff
//
// Refuses to move forward if a drop-off edge is detected with the LineFollower IR sensors.
//

void checkForDropOff()
{
  
  U_BYTE edgeState;
  
  edgeState = edgeDetector.readSensors();

  switch (edgeState)
  {
    
    case S1_IN_S2_IN:

    case S1_IN_S2_OUT:

    case S1_OUT_S2_IN:
      
      //only allow reverse direction if either sensor detect drop-off
      if(motorState != REVERSE){ motorState = STOP; }
      
      break;

    case S1_OUT_S2_OUT:
      //out means reflection is being received (out of no receive-zone)
      //so allow any direction
      break;
  }

}

// end of checkForDropOff
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// handleIRMoveCommands
//
// Moves the robot as directed from commands receive via IR.
//
// Only changes motor state if motorState has changed. This improves performance.
//

void handleIRMoveCommands()
{

  if (motorState == prevMotorState) { return; }

  prevMotorState = motorState;
  
  switch (motorState){

    case FORWARD:
      moveFwd();
      break;
      
    case REVERSE:
      moveRev();
      break;
      
    case LEFT:
      turnLeft();
      break;
      
    case RIGHT:
      turnRight();
      break;
      
    case STOP:
      stopMoving();
      break;
  }

}// end of handleIRMoveCommands
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setModeB
//
// Sets the Mode to B and makes all necessary preparations.
//

void setModeB(){

  mode = MODE_B;
  
  stopMoving();
  
  displayModeOnLed(0x0b);
  
  eyeMoveTimer = EYE_MOVE_TIME_VALUE;
  
  soundBuzzer(NTD1, 300);
  
  moveSpeed = MOVE_SPEED_MIN;

}// end of setModeB
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// processCommand
//
// Processes commands specifed by pCommand.
//

void processCommand(int pCommand){

  switch (pCommand){


    case SET_HEAD_ANGLE_CMD:

      handleSetHeadAngleCmd();
    
      break;

    case SET_LED_DIGIT_DISPLAY_CMD:
      
      handleSetLEDDigitDisplayCmd();
      
      break;

    case SET_LED_DIGIT_DISPLAY_DIRECT_CMD:
      
      handleSetLEDDigitDisplayDirectCmd();
      
      break;

    case SET_DISPLAY_STATUS_FLAGS_CMD:

      handleSetDisplayStatusFlagsCmd();

      break;
    
    case IR_BUTTON_A: //do nothing -- robot controlled only by IR remote or serial link
      
      mode = MODE_A;

      stopMoving();

      displayModeOnLed(0x0a);

      soundBuzzer(NTD2, 300);

      moveSpeed = MOVE_SPEED_MIN;
      
      break;
      
    case IR_BUTTON_B:

      setModeB();

      break;

    case IR_BUTTON_C:

      mode = MODE_C;

      stopMoving();

      displayModeOnLed(0x0c);
      
      soundBuzzer(NTD3, 300);
      
      moveSpeed = MOVE_SPEED_MIN;
      
      break;

    case IR_BUTTON_D:

      mode = MODE_D;

      stopMoving();

      displayModeOnLed(0x0d);
      
      soundBuzzer(NTD4, 300);
      
      moveSpeed = MOVE_SPEED_MIN;
      
      break;

    case IR_BUTTON_E:

      mode = MODE_E;

      stopMoving();

      displayModeOnLed(0x0e);

      soundBuzzer(NTD4, 300);

      moveSpeed = MOVE_SPEED_MID;

      distFwd = distLeft = distQLeft = distRight = distQRight = MAX_ULTRASONIC_DISTANCE;

      break;

    case IR_BUTTON_UP:  //up arrow - run forward

      motorState = FORWARD;

      break;

    case IR_BUTTON_DOWN: //down arrow - run backwards

      motorState = REVERSE;

      break;

    case IR_BUTTON_LEFT:  //turn left

      motorState = LEFT;
      
      break;

    case IR_BUTTON_RIGHT:  //turn right

      motorState = RIGHT;
      
      break;
      
    case IR_BUTTON_9:

      soundBuzzer(NTDH2, 300);
      setMoveSpeed(moveSpeedChangeStep * 9 + MOVE_SPEED_MIN);

      break;

    case IR_BUTTON_8:

      soundBuzzer(NTDH1, 300);
      setMoveSpeed(moveSpeedChangeStep * 8 + MOVE_SPEED_MIN);

      break;

    case IR_BUTTON_7:

      soundBuzzer(NTD7, 300);
      setMoveSpeed(moveSpeedChangeStep * 7 + MOVE_SPEED_MIN);

      break;

    case IR_BUTTON_6:

      soundBuzzer(NTD6, 300);
      setMoveSpeed(moveSpeedChangeStep * 6 + MOVE_SPEED_MIN);

      break;

    case IR_BUTTON_5:

      soundBuzzer(NTD5, 300);
      setMoveSpeed(moveSpeedChangeStep * 5 + MOVE_SPEED_MIN);

      break;

    case IR_BUTTON_4:

      soundBuzzer(NTD4, 300);
      setMoveSpeed(moveSpeedChangeStep * 4 + MOVE_SPEED_MIN);

      break;

    case IR_BUTTON_3:

      soundBuzzer(NTD3, 300);
      setMoveSpeed(moveSpeedChangeStep * 3 + MOVE_SPEED_MIN);

      break;

    case IR_BUTTON_2:

      soundBuzzer(NTD2, 300);
      setMoveSpeed(moveSpeedChangeStep * 2 + MOVE_SPEED_MIN);

      break;

    case IR_BUTTON_1:

      soundBuzzer(NTD1, 300);
      setMoveSpeed(moveSpeedChangeStep * 1 + MOVE_SPEED_MIN);

      break;
  }


}// end of processCommand
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// handleCommands
//
// Checks for commands from the IR receiver and serial port and processes them as required.
//
// Method ir.decode returns false if no signal received...either no button pressed or not
// receiving the signal.
//
// See notes at the top of this file explaining why the IR signal often refuses to
// change from the previous signal received.
//
// Care must be taken not to make calls to set the RGB LEDs too often or at the wrong
// times or this function will time out and return to Stop mode repeatedly. To avoid this,
// the LEDs are set elsewhere and only between mode/direction changes.
//

void handleCommands()
{
  
  static long time = millis();

  int serialCommand;

  //if ir object has a valid command then handle it
  
  if (ir.decode()){
    
    U_DWORD value = ir.value;
    time = millis();
    
    processCommand(value >> 16 & 0xff);

    return;

  }

  //handle serial input buffering
  bufferSerialInput();

  //process input packet if present
  processPacket();  
  
  if (pktReady == TRUE){

    pktReady = FALSE;

    time = millis();
    
    processCommand(pktCmd);

    //send back the current reading from the Ultrasonic sensor
    writeSerial(ultr.distanceInch(MAX_ULTRASONIC_DISTANCE));

    return;

  }

  //if no command from IR or serial for a while, then stop motors
  
  if (millis() - time > 120){
    motorState = STOP;
    time = millis();
  }

}// end of handleCommands
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setupLEDDigitDisplay
//
// Sets up the LED digit display.
//

void setupLEDDigitDisplay()
{

  digitDisplay.init();

  digitDisplay.set(BRIGHTNESS_2);

  digitDisplay.display((uint8_t)0, 1);
  digitDisplay.display((uint8_t)1, 2);
  digitDisplay.display((uint8_t)2, 3);
  digitDisplay.display((uint8_t)3, 4);

  displayStatusCodeOnLEDDigitDisp(9,8,7,6);

}// end of setupLEDDigitDisplay
//------------------------------------------------------------------------------------------


/*

//------------------------------------------------------------------------------------------
// displayNumOnDigitDisp
//
// LED 7 Segment 4 Digit Display examples.
//

void displayNumOnDigitDisp()
{

  // add 0x10 to value to turn on the decimal point for that digit

  digitDisplay.display((uint8_t)0, 1 + 0x10);
  digitDisplay.display((uint8_t)1, 2 + 0x10);
  digitDisplay.display((uint8_t)2, 3 + 0x10);
  digitDisplay.display((uint8_t)3, 4 + 0x10);

  delay(1000);

  digitDisplay.clearDisplay();

  delay(1000);

  digitDisplay.display(4321);

  delay(1000);

  digitDisplay.display(987.6);

}// end of displayNumOnDigitDisp
//------------------------------------------------------------------------------------------

*/

//------------------------------------------------------------------------------------------
// handleSetLEDDigitDisplayCmd
//
// Handles the SET_LED_DIGIT_DISPLAY_CMD command. Displays value represented by word in
// datum[0~1] on the LED digit display.
//

void handleSetLEDDigitDisplayCmd()
{

  int value;

   value = (unsigned)((datum[0]<<8) & 0xff00)
                                     + (unsigned)(datum[1] & 0xff);

  digitDisplay.display(value);

}// end of handleSetLEDDigitCmd
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// handleSetLEDDigitDisplayDirectCmd
//
// Handles the SET_LED_DIGIT_DISPLAY_DIRECT_CMD command. Displays bytes in datum[0~3] on
// the LED digit display.
//
// 0 >= datum[*] <= 0x0f  (add 0x10 to set the decimal point)
//
// If datum[*] = 255, that digit position will be left blank.
//

void handleSetLEDDigitDisplayDirectCmd()
{

  digitDisplay.clearDisplay();

  if (datum[0] != 255) { digitDisplay.display((uint8_t)0, datum[0]); }
  if (datum[1] != 255) { digitDisplay.display((uint8_t)1, datum[1]); }
  if (datum[2] != 255) { digitDisplay.display((uint8_t)2, datum[2]); }
  if (datum[3] != 255) { digitDisplay.display((uint8_t)3, datum[3]); }

}// end of handleSetLEDDigitDisplayDirectCmd
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// displayStatusCodeOnLEDDigitDisp
//
// Displays a status code comprised of 4 digits specified by pD4, pD3, pD2, pD1.
//
// The decimal point will be turned on for all digits to specify that the number displayed
// is a status code.
//

void displayStatusCodeOnLEDDigitDisp(uint8_t pD4, uint8_t pD3, uint8_t pD2, uint8_t pD1)
{

  digitDisplay.display((uint8_t) 0, pD4, (uint8_t) POINT_ON);
  digitDisplay.display((uint8_t) 1, pD3, (uint8_t) POINT_ON);
  digitDisplay.display((uint8_t) 2, pD2, (uint8_t) POINT_ON);
  digitDisplay.display((uint8_t) 3, pD1, (uint8_t) POINT_ON);

}// end of displayStatusCodeOnLEDDigitDisp
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// displayModeOnLED
//
// Clears the LED 4 digit display and displays pMode on the lowest digit.
//
// pMode -> 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
//
// Values 10-12 will be displayed as a-f.
//

void displayModeOnLed(uint8_t pMode)
{

  digitDisplay.clearDisplay();
  digitDisplay.display((uint8_t) 0, pMode);

}// end of displayModeOnLED
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setupServos
//
// Sets up the servo outputs.
//
// The mBot has several "Ports" on the board accessed via RJ25 connectors.
// A MeRJ25 Adapter is used to connect the ports to a servo drive.
// Each adapter can drive two servo motors...via connectors Slot1 and Slot2 on the adapter.
// The servo motors are powered and controlled via the 3 pin Slot* connectors...two pins
// for power and the third is a PWM signal which specifies the rotation angle of the motor.
//
// The Servo class from the Arduino library is used to control the servo motors. That
// class expects a pin number to set which pin is to be used to control the motors by
// pulses on that pin.
//
// The Makeblock libraries provide the MePort class as a convenient method of determining
// to which pin a servo is connected. The port number written on the RJ25 connector to
// which the motor is connected is passed in to a MePort constructor.
//
// If the motor is plugged into Slot1 on the MeRJ25 Adapter, then port.pin1(); is used.
// If the motor is plugged into Slot2 on the MeRJ25 Adapter, then port.pin2(); is used.
//
// For clarity, the pins are stored in variables and the following code is placed in the
// global variables section at the top of the source file:
//
//    Servo myServo1;
//    MePort port(PORT_4);
//    int16_t servo1pin =  port.pin1();//attaches to PORT_4 SLOT1
//    int16_t servo2pin =  port.pin2();//attaches to PORT_4 SLOT2
//
// The instance myServo1 is then attached to the proper pin using:
//    myServo1.attach(servo1pin);
//

void setupServos()
{

  frontUTServo.attach(servo1pin);  // attach the servo to the appropriate pin

}// end of setupServos
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// oscillateFrontUTServo
//
// Oscillates the servo a few times.
//

void oscillateFrontUTServo()
{

  for(int i=0; i<2; i++){

    frontUTServo.write(EYES_Q_RIGHT);
    delay(1000);
    frontUTServo.write(EYES_RIGHT);
    delay(1000);
    frontUTServo.write(EYES_Q_RIGHT);
    delay(1000);

    frontUTServo.write(EYES_FORWARD);
    delay(1000);

    frontUTServo.write(EYES_Q_LEFT);
    delay(1000);
    frontUTServo.write(EYES_LEFT);
    delay(1000);
    frontUTServo.write(EYES_Q_LEFT);
    delay(1000);

    frontUTServo.write(EYES_FORWARD);
    delay(1000);

  }

}// end of oscillateFrontUTServo
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// turnEyesAndMeasure
//
// Turns the eyes to dAngle, pauses to allow for servo settling, reads and returns the
// target distance from the ultrasonic sensor.
//
// In the sample code, a zero returned from the ultrasonic sensor was treated as invalid.
// The same is done here, but thorough testing of that assumption has not been done.
//
// Displays the current distance measurement in the lower 2 digits of the LED display.
// Displays the current moveMode in the upper two digits.
//

int turnEyesAndMeasure(int dAngle)
{

    frontUTServo.write(dAngle);
    delay(300);
    int distance = ultr.distanceInch(MAX_ULTRASONIC_DISTANCE);
    if (distance == 0) { distance = MAX_ULTRASONIC_DISTANCE; } //catch illegal zero
    digitDisplay.display( moveMode * 100 + distance);
    return(distance);

}// end of turnEyesAndMeasure
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// turnEyesPeriodically
//
// Periodically turns the ultrasonic sensor "eyes" to different angles and obtains a
// distance reading and stores it in the appropriate variable for the current view
// angle.
//

void turnEyesPeriodically() {

  if(--eyeMoveTimer != 0) { return; }

  eyeMoveTimer = EYE_MOVE_TIME_VALUE;

  switch (eyePosition)
  {

    case 0:
      distQLeft = turnEyesAndMeasure(EYES_Q_LEFT);
      eyePosition = 1;
    break;

    case 1:
      distFwd = turnEyesAndMeasure(EYES_FORWARD);
      eyePosition = 2;
    break;

    case 2:
      distQRight = turnEyesAndMeasure(EYES_Q_RIGHT);
      eyePosition = 3;
    break;

    case 3:
      distRight = turnEyesAndMeasure(EYES_RIGHT);
      eyePosition = 4;
    break;

    case 4:
      distQRight = turnEyesAndMeasure(EYES_Q_RIGHT);
      eyePosition = 5;
    break;

    case 5:
      distFwd = turnEyesAndMeasure(EYES_FORWARD);
      eyePosition = 6;
    break;

    case 6:
      distQLeft = turnEyesAndMeasure(EYES_Q_LEFT);
      eyePosition = 7;
    break;

    case 7:
      distLeft = turnEyesAndMeasure(EYES_LEFT);
      eyePosition = 0;
    break;

    default:
      distFwd = turnEyesAndMeasure(EYES_FORWARD);
      eyePosition = 6;
    break;

  }

}// end of turnEyesPeriodically
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// setup
//

void setup() {

  randomSeed(analogRead(6));

  eyeMoveTimer = EYE_MOVE_TIME_VALUE;
  eyePosition = 6; //eyes start pointing forward

  Serial.begin(57600);

//debug mks -- remove this test
    soundBuzzer(400, 300);
    delay(100);
    soundBuzzer(300, 300);
    delay(100);
    soundBuzzer(200, 300);
    delay(100);
    soundBuzzer(100, 300);
    delay(3000);
//debug mks end  


  Serial.print("\n");

//debug mks -- remove this test
  if (Serial.available()){
    soundBuzzer(400, 300);
    delay(100);
    soundBuzzer(300, 300);
    delay(100);
    soundBuzzer(200, 300);
    delay(100);
    soundBuzzer(100, 300);
    delay(100);
  }
//debug mks end  

  ir.begin();

  setupLEDDigitDisplay();

  setupServos();

  frontUTServo.write(EYES_FORWARD);

  setUpRGBLeds();

  buzzer.setpin(8);

  soundBuzzer(100, 300);

  testRGBLEDs();

  soundBuzzer(100, 300);

  cycleColorForLED(RIGHT_RGB_LED);

  setRGBLEDsStopMode();

  soundBuzzer(100, 300);

  displayModeOnLed(0x0a); //starts in Mode A

}// end of setup
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// loop
//

void loop() {

  handleCommands();

  //checkForDropOff(); //doesn't work if table is not reflective enough

  handleIRMoveCommands();

  if (mode == MODE_B){  }
  else
  if (mode == MODE_C){  }
  else
  if (mode == MODE_D){  }
  else
  if (mode == MODE_E){  }

  // move next section to a function

  currentPressed = !(analogRead(7) > 100);

  //sound buzzer if button on mCore board pressed
  
  if(currentPressed != prevButtonState){
    
    prevButtonState = currentPressed;

    if (currentPressed){
      soundBuzzer(100, 300);
    }
  }

  //move next section end
    
//  delay(300);

}// end of loop
//------------------------------------------------------------------------------------------

